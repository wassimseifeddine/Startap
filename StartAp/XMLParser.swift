//
//  XMLParser.swift
//  StartAp
//
//  Created by Wassim Seifeddine on 5/8/16.
//  Copyright © 2016 Wassim Seifeddine. All rights reserved.
//

import Foundation
/// The class that will fetch the feed and parse the XML content
class XMLParser:  NSObject, NSXMLParserDelegate {
    
    
    /// URL of the feed
    var RSSURL : String
    
    init(RSSURL : String) {
        self.RSSURL = RSSURL
    }
    
    /// Will contain all the headlines fetched
    var headlines = [Headline]()
    /// A Dummy headline used to parse
    var headline = Headline()
    
    
    /// Used for parsing
    var currentElement = ""
    /// Used for parsing
    var foundCharacters = ""
    
    /**
     Used to check if the current headline is finisehd to insert into the array
     
     - returns: True is finished. False if one field is still not set
     */
    func didFinishConfiguringHeadline() ->Bool {
        return headline.description != nil && headline.title != nil && headline.link != nil
    }
    
    
    
    /**
     Fetches the Feed as NSData and will start parsing it
     */
    func startParsing(){
        
        
        let parser = NSXMLParser(contentsOfURL: NSURL(string: RSSURL)!)
        parser!.delegate = self
        parser!.parse()
        
    }
    
    //MARK:- NSXMLParser delegate
    /**
     The delegate method that will inform the pars
     
     
     */
    func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        currentElement = elementName
    }
    
    func parser(parser: NSXMLParser, foundCharacters string: String) {
        if  currentElement == "title"  || currentElement == "link" || currentElement == "description"{
            foundCharacters += string
            
        }
    }
    
    
    
    func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        
        
        
        
        if didFinishConfiguringHeadline(){
            headlines.append(headline)
            
            headline.reset()
            return
            
        }
        
        
        
        
        switch currentElement {
        case "link":
            
            foundCharacters = (foundCharacters as NSString).substringFromIndex(3)
            headline.link  = foundCharacters
            break
            
        case "title" :
            
            
            headline.title = foundCharacters
            break
        case "description" :
            
            headline.description = foundCharacters
            break
        default:
            
            break
        }
        
        foundCharacters = ""
        
        
        
        
        
    }
    
    func parserDidEndDocument(parser: NSXMLParser) {
        
        
    }
}
