//
//  PublicMethods.swift
//  StartAp
//
//  Created by Wassim Seifeddine on 5/8/16.
//  Copyright © 2016 Wassim Seifeddine. All rights reserved.
//

import Foundation
import UIKit
import SCLAlertView
import Material

/// Contains the methods that will be used in a simila manner throughout the app 
class PublicMehods {
    
    /**
     Displays the alert view that is used throughout the app
     
     - parameter title:    The title that will be used in the alert view
     - parameter subtitle: The Subtitle that will be used in the alert view
     */
    
     static func showAlertView( target : SCLAlertViewStyle , title : String , subtitle : String ){
        
        var colorStyle : UInt?
        let colorTextButton : UInt? = UInt( Constants.Colors.white)
        switch target {
        case .Success:
            colorStyle = UInt(Constants.Colors.green)
        case .Error :
            colorStyle = UInt(Constants.Colors.red)
        case .Warning:
            colorStyle = UInt(Constants.Colors.orange)
        default:
            print("Default")
        }
        
        SCLAlertView().showTitle(
            title, // Title of view
            subTitle: subtitle, // String of view
            duration: 0.0, // Duration to show before closing automatically, default: 0.0
            completeText: "Done", // Optional button value, default: ""
            style: target, // Styles - see below.
            colorStyle: colorStyle,
            colorTextButton: colorTextButton
        )
    }
    
    
     static func showActivityIndicator(location : CGPoint) -> MKActivityIndicator{
        let activityIndicator = MKActivityIndicator(frame: CGRect(x: location.x - 50    , y: location.y - 50 , width: 100, height:100))
        activityIndicator.color = UIColor(netHex: Int(Constants.Colors.blue))
        activityIndicator.lineWidth = Constants.activityIndicatorLineWidth
        return activityIndicator
        
    }

    
   
}

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    /**
     Extension to the UIColor class that will allow to initiate a color using hexadecimal number
     
     - parameter netHex: The Hexadecimal number
     
     */
    convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
    
}
