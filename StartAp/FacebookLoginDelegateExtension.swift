//
//  FacebookLoginDelegateExtension.swift
//  StartAp
//
//  Created by Wassim Seifeddine on 5/10/16.
//  Copyright © 2016 Wassim Seifeddine. All rights reserved.
//

import Foundation
import FBSDKLoginKit

extension mainViewController{
    
    
    /**
     Triggered when the logout button has been pressed and the user was logged out
     
     - parameter loginButton: The FBSDKLoginButton object
     */
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        
        facebookPageTextFieldOutlet.enabled = false
        facebookPageTextFieldOutlet.text = ""
        facebookLikeButton.enabled = false
    }
    
    /**
     Triggered just before the logging in happens
     
     - parameter loginButton: The FBSDKLoginButton object
     
     - returns: True to continue logging in. False to stop
     */
    func loginButtonWillLogin(loginButton: FBSDKLoginButton!) -> Bool {
        
        return true
        
    }
    
    /**
     Triggered after the logging in happens
     
     - parameter loginButton: The FBSDKLoginButton object
     - parameter result:      The result of the logging in
     - parameter error:       The error object 
     */
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        
        
        print("Logged in from delegate")
    }
}