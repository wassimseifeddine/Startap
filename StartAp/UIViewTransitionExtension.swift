//
//  UIViewTransitionExtension.swift
//  StartAp
//
//  Created by Wassim Seifeddine on 5/11/16.
//  Copyright © 2016 Wassim Seifeddine. All rights reserved.
//

import Foundation



// Usage: insert view.pushTransition right before changing content
extension UIView {
    func pushTransition(duration:CFTimeInterval) {
        let animation:CATransition = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name:
            kCAMediaTimingFunctionEaseInEaseOut)
        animation.type = kCATransitionPush
        animation.subtype = kCATransitionFromTop
        animation.duration = duration
        self.layer.addAnimation(animation, forKey: kCATransitionPush)
    }
}