//
//  mainViewController.swift
//  StartAp
//
//  Created by Wassim Seifeddine on 5/8/16.
//  Copyright © 2016 Wassim Seifeddine. All rights reserved.
//

import UIKit
import Material
import Spring
import FBSDKLoginKit
import FBSDKCoreKit
import FBSDKShareKit

/// Controls the main view of the app
class mainViewController: UIViewController, UITextFieldDelegate, FBSDKLoginButtonDelegate {
    
    //Variables
    let rssHD = RSSHandler()
    let cardView : CardView = CardView()
    let queue = NSOperationQueue()
    var timerForFetchingHeadlines = NSTimer()
    var timerForAnimatingHeadlines = NSTimer()
    var headlineIndex : Int = 1
    var currenHeadline : Headline?
    var FacebookHD : FacebookHandler?
    
    
    //Outlets
    @IBOutlet weak var newsLabelOutlet : UILabel!
    @IBOutlet weak var facebookPageTextFieldOutlet : TextField!
    @IBOutlet weak var facebookLikeButton : FBSDKLikeButton!
    @IBOutlet weak var facebookLoginButton : FBSDKLoginButton!
    @IBOutlet weak var activityIndicator : MKActivityIndicator!
    
    
    
    //MARK:-Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareView()
        facebookPageTextFieldOutlet.delegate = self
        facebookLoginButton.delegate = self
        subscribeToKeyboardNotifications()
        fetchHeadlines()
        initializeTextField(facebookPageTextFieldOutlet)
        
        activityIndicator.startAnimating()
        activityIndicator.lineWidth = Constants.activityIndicatorLineWidth
        activityIndicator.backgroundColor = nil
        
        
        
    }
    
    
    
    
    /**
     Handles when a news headline if tapped. Displays a card view with description
     
     - parameter gestureRecognizer: The UITapGestureRecognizer
     */
    func newsLabelTapped(gestureRecognizer: UITapGestureRecognizer){
        
        let dismissButton: FlatButton = FlatButton()
        let openLinkButton: FlatButton = FlatButton()
        /**
         Dismiss button.
         */
        dismissButton.pulseColor = MaterialColor.blue.lighten1
        dismissButton.setTitle("Dismiss", forState: .Normal)
        dismissButton.setTitleColor(MaterialColor.blue.darken1, forState: .Normal)
        dismissButton.addTarget(self, action: #selector(mainViewController.dismissCardView), forControlEvents: .TouchUpInside)
        
        
        /**
         Open Link button.
         */
        openLinkButton.pulseColor = MaterialColor.blue.lighten1
        openLinkButton.setTitle("More Info", forState: .Normal)
        openLinkButton.setTitleColor(MaterialColor.blue.darken1, forState: .Normal)
        openLinkButton.addTarget(self, action: #selector(mainViewController.openLink), forControlEvents: .TouchUpInside)
        
        let titleLabelDer : UILabel = UILabel()
        
        
        let detailLabel: UILabel = UILabel()
        
        /**
         Title label customization
         */
        titleLabelDer.adjustsFontSizeToFitWidth = true
        titleLabelDer.textColor = MaterialColor.blue.darken1
        titleLabelDer.font = RobotoFont.mediumWithSize(20)
        titleLabelDer.text = currenHeadline!.title
        cardView.titleLabel = titleLabelDer
        
        
        
        /**
         Detail label customization
         */
        detailLabel.text = currenHeadline!.description!
        detailLabel.numberOfLines = 0
        cardView.detailView = detailLabel
        
        
        
        /**
         Adde buttons to Card View
         */
        cardView.leftButtons = [dismissButton]
        cardView.rightButtons  = [openLinkButton]
        
        view.addSubview(cardView)
        cardView.translatesAutoresizingMaskIntoConstraints = false
        MaterialLayout.alignFromTop(view, child: cardView, top: 100)
        MaterialLayout.alignToParentHorizontally(view, child: cardView, left: 20, right: 20)
        
        
    }
    
    
    /**
     Addes a UITapGestureRecognizer to the news label
     */
    func addGestureRecognizerToNewsLabel(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(mainViewController.newsLabelTapped))
        tapGesture.numberOfTapsRequired = 1
        newsLabelOutlet.addGestureRecognizer(tapGesture)
        newsLabelOutlet.userInteractionEnabled = true
        
    }
    
    /**
     Dismisses the card view when the dismiss button on the card view
     */
    func dismissCardView() {
        cardView.removeFromSuperview()
    }
    
    
    
    /**
     Sets the textField attributes to match the application design pattern
     
     - parameter textField: The TextField object to be set
     
     */
    func initializeTextField (textField : TextField){
        textField.placeholderColor = MaterialColor.grey.base
        textField.font = RobotoFont.regularWithSize(20)
        textField.textColor = UIColor(netHex: 0x424242)
        textField.backgroundColor = nil
        
        textField.placeholder = "Facebook page"
    }
    
    /**
     Gets the latest headline from RSS feed
     */
    private func getLatestHeadlines(){
        
        let xmlP = XMLParser(RSSURL: Constants.RSSURLS.manar)
        xmlP.startParsing()
        print("I got \(xmlP.headlines.count) items")
        
        rssHD.headlines = xmlP.headlines
        
    }
    /**
     Calls 'getLatestHeadlines' and manages background queues
     */
    func fetchHeadlines(){
        
        queue.addOperationWithBlock() {
            self.getLatestHeadlines()
            
            NSOperationQueue.mainQueue().addOperationWithBlock() {
                
               
                self.initiateTimers()
                
                
                
            }
        }
        
    }
    /**
     Opens the current headline link in safari
     */
    func openLink(){
        UIApplication.sharedApplication().openURL(NSURL(string: currenHeadline!.link!)!)
    }
    
    /**
     updates the newsLabel text to match the current headline
     */
    private func updateUI(){
        self.addGestureRecognizerToNewsLabel()
        newsLabelOutlet.pushTransition(0.4)
        newsLabelOutlet.text = currenHeadline!.title!
        
    }
    
    /**
     Manages the headline's transmission
     */
    func animateHeadline(){
        
        currenHeadline =  rssHD.getHeadline(headlineIndex)
        headlineIndex =  ((headlineIndex + 1) % 12 )
        headlineIndex %= 11
        
        updateUI()
        
        
    }
    
    /**
     Prepares the view and its components
     */
    func prepareView(){
        view.backgroundColor = UIColor(netHex: Constants.Colors.backgroundWhite)
        newsLabelOutlet.text = "Getting Latest Headlines"
        newsLabelOutlet.adjustsFontSizeToFitWidth = true
        
    }
    
    /**
     Initiiate the NSTimers that will trigger the headlines's animaion the fetching
     
     
     */
    func initiateTimers(){
        
        animateHeadline()
        self.activityIndicator.stopAnimating()
        timerForAnimatingHeadlines = NSTimer.scheduledTimerWithTimeInterval(3.0, target: self, selector: #selector(mainViewController.animateHeadline), userInfo: nil, repeats: true)
        timerForFetchingHeadlines = NSTimer.scheduledTimerWithTimeInterval(3600, target: self, selector: #selector(mainViewController.fetchHeadlines), userInfo: nil, repeats: true)
        
    }

    

    
    
    
    
}













