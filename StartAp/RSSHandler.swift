//
//  RSSHandler.swift
//  StartAp
//
//  Created by Wassim Seifeddine on 5/8/16.
//  Copyright © 2016 Wassim Seifeddine. All rights reserved.
//

import UIKit


/// Handles the response that will be receied from RSS
class RSSHandler : NSObject {
    
    var headlines = [Headline]()
    /**
     Returns a specific headline object
     
     - parameter index: The index of the headline to be returned
     
     - returns: the requested headline
     */
    func getHeadline(index : Int) -> Headline{
        return headlines[index]
    }
    
   
    
}
