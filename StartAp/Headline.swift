//
//  Headline.swift
//  StartAp
//
//  Created by Wassim Seifeddine on 5/8/16.
//  Copyright © 2016 Wassim Seifeddine. All rights reserved.
//

import Foundation
/**
 *   The Model for the headline
 */
struct Headline {
    var title : String?
    var link : String?
    var description : String?
    

}

extension Headline {
    /**
     Extension for the Headline struct for setting all fields to nil after inserting into array 
     */
    mutating func reset() {
        title = nil
        link = nil
        description = nil 
    }
}
