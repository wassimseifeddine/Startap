//
//  FacebookHandler.swift
//  StartAp
//
//  Created by Wassim Seifeddine on 5/9/16.
//  Copyright © 2016 Wassim Seifeddine. All rights reserved.
//

import Foundation
import FBSDKLoginKit
import FBSDKCoreKit


class FacebookHandler {
    
    
    class func sharedInstance() -> FacebookHandler {
        struct Static {
            static let instance = FacebookHandler()
        }
        
        return Static.instance
    }
    
    
    
    
    /**
     Starts a facebook login procedure
     
     - parameter callback: The callback when login is successful
     
     */
    func login(callback : (result : FBSDKLoginManagerLoginResult) ->Void ){
        let FBLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        
        
        FBLoginManager.logInWithReadPermissions([Constants.FBSDKConstants.permissions.email, Constants.FBSDKConstants.permissions.userLikes], fromViewController: nil, handler: { (result :FBSDKLoginManagerLoginResult!, error :NSError!) in
            if error == nil {
                print("Login Succesful ")
                callback(result: result)
            }else {
                print(error)
            }
        })
        
        
        
    }
    
    func requestMorePermission(){
        let FBLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        FBLoginManager.logInWithPublishPermissions([Constants.FBSDKConstants.permissions.publicActions], fromViewController: nil) { (result :FBSDKLoginManagerLoginResult!, error :NSError!) in
            if error == nil {
                print("Login Succesful with publish permissions")
                
            }else {
                print(error)
            }
        }
        
    }
    func logout(){
         let FBLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        
        FBLoginManager.logOut()
    }
    
    func userDoesLikePage(pageId : String,callback : (result :AnyObject? )->Void){
        
        let params = ["fields" : Constants.FBSDKConstants.parameters.id]
        FBSDKGraphRequest(graphPath: "me/likes/\(pageId)", parameters: params).startWithCompletionHandler({ (connection :FBSDKGraphRequestConnection!, result :AnyObject!, error :NSError!) in
            if error == nil {
                
                if let result = result , data = result["data"] {
                    
                    print(result)
                    
                    if data!.count > 0 {
                        
                        callback(result: data![0]["id"])
                        
                    }else{
                        callback(result: nil)
                    }
                }
                
                
            }else{
                print("error \(error.localizedDescription)")
            }
        })
    }
    
    
    

 


}