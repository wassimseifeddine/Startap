//
//  ExtensionToMainView.swift
//  StartAp
//
//  Created by Wassim Seifeddine on 5/10/16.
//  Copyright © 2016 Wassim Seifeddine. All rights reserved.
//

import Foundation

extension mainViewController {
    /**
     Subscribes on NSNotication and listens on the keyboardWillHide notification
     
     */
    func subscribeToKeyboardNotifications() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(mainViewController.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(mainViewController.KeyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
    }
   
    /**
     Unsubscribes on NSNotication 
     */
    func unsubscribeToKeyboardNotifications(){
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    /**
     This function will do the necessary view changed when the keybaord will appear
     
     - parameter notification: the notification that informs that the keyboard will show
     */
    func keyboardWillShow(notification: NSNotification) {
        if facebookPageTextFieldOutlet
            .isFirstResponder(){
            view.frame.origin.y -= getKeyboardHeight(notification)
        }
    }
    /**
     This function will do the necessary view changed when the keybaord will hide
     
     
     - parameter notification: the notification that informs that the keyboard will show
     */
    func KeyboardWillHide (notification : NSNotification){
        
        view.frame.origin.y = 0
    }
    /**
     This function will return the keyboard height
     
     - parameter notification: the notification that informs that the keyboard will show
     
     - returns: The keyboard height
     */
    func getKeyboardHeight(notification: NSNotification) -> CGFloat {
        let userInfo = notification.userInfo!
        let keyboardSize = userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue // of CGRect
        return keyboardSize.CGRectValue().height
    }
    
    
}