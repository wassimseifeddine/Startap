//
//  UITextFieldDelegateExtension.swift
//  StartAp
//
//  Created by Wassim Seifeddine on 5/11/16.
//  Copyright © 2016 Wassim Seifeddine. All rights reserved.
//

import Foundation

extension mainViewController{
    
    //MARK:- UITextFieldDelegate
    func textFieldDidEndEditing(textField: UITextField) {
        if textField.text!.isEmpty {
            facebookLikeButton.enabled = false
            return
        }
        facebookLikeButton.enabled = true
        facebookLikeButton.objectID = textField.text!
    }
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
