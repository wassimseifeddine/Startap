//
//  Constants.swift
//  StartAp
//
//  Created by Wassim Seifeddine on 5/8/16.
//  Copyright © 2016 Wassim Seifeddine. All rights reserved.
//

import Foundation

/**
 *   Contains the constants of the app
 */
public struct Constants{
    /**
     *   Contains the hexadecimal numbers of material colors
     */
    struct Colors {
        static let green : Int = 0x4CAF50
        static let white  : Int = 0xFFFFFF
        static let red : Int = 0xef5350
        static let orange : Int = 0xFB8C00
        static let backgroundWhite : Int = 0xEEEEEE
        
        static let blue : Int = 0x2196F3
        
    }
    /**
     *   Contains identifiers for the app GCD queues
     */
    struct Queues {
        static let userInteractive = Int(QOS_CLASS_USER_INTERACTIVE.rawValue)
        static let background = Int(QOS_CLASS_BACKGROUND.rawValue)
    }
    
    /**
     *   Contains the RSS url for fetching the feed
     */
    struct RSSURLS {
        static let manar : String  = "http://www.almanar.com.lb/rss"
    }
    
   
    
    struct FBSDKConstants {
        struct parameters {
            static let id = "id"
            static let name = "name"
            static let about = "about"
        }
        
        struct permissions {
            static let email = "email"
            static let userLikes = "user_likes"
            static let publicActions = "publish_actions"
            
        }
    }
    static let activityIndicatorLineWidth : CGFloat = 2.0
    
    
    
}