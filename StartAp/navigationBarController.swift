//
//  navigationBarController.swift
//  StartAp
//
//  Created by Wassim Seifeddine on 5/8/16.
//  Copyright © 2016 Wassim Seifeddine. All rights reserved.
//
import UIKit
import Material

/// Controls the navigation bar of the app's navigation controller
class navigationBarController: NavigationBar {
    
    override func prepareView() {
        
        backButtonImage = MaterialIcon.arrowBack
        backButton.pulseColor = UIColor(netHex: Constants.Colors.backgroundWhite)
        backButton.tintColor = MaterialColor.white
        
        
        barTintColor = UIColor(netHex: Constants.Colors.blue)
        tintColor = nil
    }
    
}
